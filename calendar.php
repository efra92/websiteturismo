<!doctype html>
<html lang="es">
	<head>
		<title>&copy;Web oficial Edma Events</title>
		<link rel="shortcut icon" href="./favicon.ico">
	</head>
	<body>
		<section id="wrapper">
			<section id="sueno">
				<img src="./styles/img/realidad.png"/>
			</section>
			<header>
				<?php
					include('./scripts/php/header.php');
				?>
				<section id="menus">
				<?php
					include('./scripts/php/menu.php');
				?>
				</section>
			</header>
			<section id='content'>
				<section id='calendario'>
					<section id="calendar">					
			<section id="calcontainer">
				<section id="calheader">
					<h4>Marzo 2013</h4>
				</section>		
				<section id="dayssemana">
					<section class="daysemana"><p>Lunes</p></section>
					<section class="daysemana"><p>Martes</p></section>
					<section class="daysemana"><p>Mi&eacute;rcoles</p></section>
					<section class="daysemana"><p>Jueves</p></section>
					<section class="daysemana"><p>Viernes</p></section>
					<section class="daysemana"><p>S&aacute;bado</p></section>
					<section class="daysemana"><p>Domingo</p></section>
				</section>
				<section id="daysmonth">
<!---------------------------------------- semana 1 ---------------------------------------->
					<section class="semana">
						<section class="day">
							<section class="daybar"><p>25</p></section>
							<section class="dots">
								<ul></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>		
						<section class="day">
							<section class="daybar"><p>26</p></section>
							<section class="dots">
								<ul></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day">
							<section class="daybar"><p>27</p></section>
							<section class="dots">
								<ul></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day">
							<section class="daybar"><p>28</p></section>
							<section class="dots">
								<ul></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>	
						<section class="day">
							<section class="daybar"><p>1</p></section>
							<section class="dots">
								<ul>
									<li class="green"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="green l2 a8"><p>17:00 Bautizo de Ana Hern�ndez</p></li>	
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day">
							<section class="daybar"><p>2</p></section>
							<section class="dots">
								<ul><li class="red"></li></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul><li class="red l3 a17"><p>00:00 Comienzo evento en discoteca Joy. </p></li></ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>3</p></section>
							<section class="dots">
								<ul>
									<li class="blue"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="blue l2 a1"><p>12:00 Boda de Paco y Laura.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
					</section>	
<!---------------------------------------- semana 2 ---------------------------------------->
					<section class="semana">
						<section class="day">
							<section class="daybar"><p>4</p></section>
							<section class="dots">
								<ul></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>		
						<section class="day">
							<section class="daybar"><p>5</p></section>
							<section class="dots">
								<ul>
									<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l2 a12"><p>19:00 Fiesta Privada de Alex.</p></li>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day">
							<section class="daybar"><p>6</p></section>
							<section class="dots">
								<ul>	</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day">
							<section class="daybar"><p>7</p></section>
							<section class="dots">
								<ul></ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul></ul>
							</section>	
							<!-- slide cerrado -->
						</section>	
						<section class="day">
							<section class="daybar"><p>8</p></section>
							<section class="dots">
								<ul>
									<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l2 a15"><p>22:00 Verbena en el Camino Polo.</p></li>					
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day">
							<section class="daybar"><p>9</p></section>
							<section class="dots">
								<ul>
									<li class="yellow"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="yellow l2 a7"><p>14:00 Comuni�n de Pedro y Pablo.</p></li>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>10</p></section>
							<section class="dots">
								<ul>
									<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l2 a0"><p>11:00 Cumplea�os de Alfredo.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
					</section>	
<!---------------------------------------- semana 3 ---------------------------------------->
					<section class="semana">
						<section class="day brn">
							<section class="daybar"><p>&nbsp;11</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>		
						<section class="day brn">
							<section class="daybar"><p>12</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>&nbsp;13</p></section>
							<section class="dots">
								<ul>
									&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>						
									<li class="red l2 a13"><p>20:00 Cena de Empresa.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day brn">
							<section class="daybar"><p>14</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>	
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;15</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l3 a15"><p>22:00 Concierto de Pepe Benavente en Valle Guerra.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;16</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;<li class="blue"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="blue l2 a7"><p>13:00 Boda de Jose y Ana.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;&nbsp;17</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l2 a16"><p>23:00 Fiesta Privada de Laura.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
					</section>	
<!---------------------------------------- semana 4 ---------------------------------------->
					<section class="semana">
						<section class="day brn">
							<section class="daybar"><p>&nbsp;18</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>		
						<section class="day brn">
							<section class="daybar"><p>19</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>&nbsp;20</p></section>
							<section class="dots">
								<ul>
									&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>						
									<li class="red l2 a13"><p>20:00 Cena de Empresa.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day brn">
							<section class="daybar"><p>21</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>	
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;22</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l3 a15"><p>22:00 Concierto de Pepe Benavente en Valle Guerra.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;23</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;<li class="blue"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="blue l2 a7"><p>13:00 Boda de Jose y Ana.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;&nbsp;24</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l2 a16"><p>23:00 Fiesta Privada de Laura.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
					</section>		
<!---------------------------------------- semana 5 ---------------------------------------->
					<section class="semana">
						<section class="day brn">
							<section class="daybar"><p>&nbsp;25</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>		
						<section class="day brn">
							<section class="daybar"><p>26</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>&nbsp;27</p></section>
							<section class="dots">
								<ul>
									&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>						
									<li class="red l2 a13"><p>20:00 Cena de Empresa.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day brn">
							<section class="daybar"><p>28</p></section>
							<section class="dots">
								<ul>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>	
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;29</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l3 a15"><p>22:00 Concierto de Pepe Benavente en Valle Guerra.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;30</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;<li class="blue"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="blue l2 a7"><p>13:00 Boda de Jose y Ana.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>				
						<section class="day brn">
							<section class="daybar"><p>&nbsp;&nbsp;&nbsp;31</p></section>
							<section class="dots">
								<ul>
									&nbsp;&nbsp;&nbsp;<li class="red"></li>
								</ul>
							</section>	
							<!-- slide abierto -->
							<section class="abierto">
								<ul>
									<li class="red l2 a16"><p>23:00 Fiesta Privada de Laura.</p></li>						
								</ul>
							</section>	
							<!-- slide cerrado -->
						</section>			
					</section>	
<!---------------------------------------- semana 5 end ---------------------------------------->
				</section>				
			</section>					
			<section id="calcat">
				<section class="caldot blue"></section><p>Bodas</p>
				<section class="caldot yellow"></section><p>Comuniones</p>
				<section class="caldot green"></section><p>Bautizos</p>
				<section class="caldot red"></section><p>Fiestas</p><br>

			</section>				
		</section>	
				</section>
			</section>
			<footer>
				<?php
					include('./scripts/php/footer.php');
				?>
			</footer>
		</section>
		<?php
			include('./scripts/php/scripts.php');
		?>
	</body>
</html>