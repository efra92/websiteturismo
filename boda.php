<!doctype html>
<html lang="es">
	<head>
		<title>&copy;Web oficial Edma Events</title>
		<link rel="shortcut icon" href="./favicon.ico">
	</head>
	<body>
		<section id="wrapper">
			<section id="sueno">
				<img src="./styles/img/realidad.png"/>
			</section>
			<header>
				<?php
					include('./scripts/php/header.php');
				?>
				<section id="menus">
				<?php
					include('./scripts/php/menu.php');
				?>
				</section>
			</header>
			<section id='content'>
				<section>
					<blockquote> Toda pareja sue�a con que el d�a de su boda sea un d�a perfecto, un momento especial que perdure en sus memorias. Nosotros haremos que ese sue�o se haga realidad y que nunca lo puedan olvidar.</blockquote>
				</section>
				<section id="tabs" style="margin:20px;">
					<ul>
					<li><a href="#tabs-1">Im�genes Pack Ahorro</a></li>
					<li><a href="#tabs-2">Im�genes Pack B�sico</a></li>
					<li><a href="#tabs-3">Im�genes Pack Lujo</a></li>
					</ul>
					<section id="tabs-1">
						<section style="text-align:center"><a href="./styles/img/imgBoda/ahorro/1.jpg" rel="lightbox[ahorro]"><img src="./styles/img/imgBoda/ahorro/1_min.jpg" /></a> <a href="./styles/img/imgBoda/ahorro/2.jpg" rel="lightbox[ahorro]"><img src="./styles/img/imgBoda/ahorro/2_min.jpg" /></a> <a href="./styles/img/imgBoda/ahorro/6.jpg" rel="lightbox[ahorro]"><img src="./styles/img/imgBoda/ahorro/6_min.jpg" /></a> <a href="./styles/img/imgBoda/ahorro/3.jpg" rel="lightbox[ahorro]"><img src="./styles/img/imgBoda/ahorro/3_min.jpg" /></a> <a href="./styles/img/imgBoda/ahorro/4.jpg" rel="lightbox[ahorro]"><img src="./styles/img/imgBoda/ahorro/4_min.jpg" /></a> <a href="./styles/img/imgBoda/ahorro/5.jpg" rel="lightbox[ahorro]"><img src="./styles/img/imgBoda/ahorro/5_min.jpg" /></a></section>
					</section>
					<section id="tabs-2">
						<section style="text-align:center"><a href="./styles/img/imgBoda/basico/1.jpg" rel="lightbox[basico]"><img src="./styles/img/imgBoda/basico/1_min.jpg" /></a> <a href="./styles/img/imgBoda/basico/2.jpg" rel="lightbox[basico]"><img src="./styles/img/imgBoda/basico/2_min.jpg" /></a> <a href="./styles/img/imgBoda/basico/3.jpg" rel="lightbox[basico]"><img src="./styles/img/imgBoda/basico/3_min.jpg" /></a> <a href="./styles/img/imgBoda/basico/4.jpg" rel="lightbox[basico]"><img src="./styles/img/imgBoda/basico/4_min.jpg" /></a> <a href="./styles/img/imgBoda/basico/5.jpg" rel="lightbox[basico]"><img src="./styles/img/imgBoda/basico/5_min.jpg" /></a> <a href="./styles/img/imgBoda/basico/6.jpg" rel="lightbox[basico]"><img src="./styles/img/imgBoda/basico/6_min.jpg" /></a></section>
					</section>
					<section id="tabs-3">
						<section style="text-align:center"><a href="./styles/img/imgBoda/lujo/1.jpg" rel="lightbox[lujo]"><img src="./styles/img/imgBoda/lujo/1_min.jpg" /></a> <a href="./styles/img/imgBoda/lujo/2.jpg" rel="lightbox[lujo]"><img src="./styles/img/imgBoda/lujo/2_min.jpg" /></a> <a href="./styles/img/imgBoda/lujo/3.jpg" rel="lightbox[lujo]"><img src="./styles/img/imgBoda/lujo/3_min.jpg" /></a> <a href="./styles/img/imgBoda/lujo/4.jpg" rel="lightbox[lujo]"><img src="./styles/img/imgBoda/lujo/4_min.jpg" /></a> <a href="./styles/img/imgBoda/lujo/5.jpg" rel="lightbox[lujo]"><img src="./styles/img/imgBoda/lujo/5_min.jpg" /></a> <a href="./styles/img/imgBoda/lujo/6.jpg" rel="lightbox[lujo]"><img src="./styles/img/imgBoda/lujo/6_min.jpg" /></a></section>
					</section>
				</section>
				<table class="pricing-table" align="center">
					<thead>
						<tr class="plan">
							<td class="orange">
								<h2>PACK AHORRO</h2>
								<em>Eligenos, no te arrepentir�s.</em>
							</td>
							<td class="orange">
								<h2>PACK BASICO</h2>
								<em>Calidad/precio irresistible.</em>
							</td>
							<td class="orange">
								<h2>PACK DE LUJO</h2>
								<em>Realizamos las mejores fiestas.</em>
							</td>
						</tr>
						<tr class="price">
							<td class="orange">
								<p><span>�</span>2000</p>
								<span>50 invitados</span>
							</td>
							<td class="green">
								<p><span>�</span>5000</p>
								<span>100 invitados</span>
							</td>
							<td class="orange">
								<p><span>�</span>10000</p>
								<span>150 invitados</span>
							</td>
						</tr>
					</thead>
				 
					<tbody>
						<tr class="clock-icon">
							<td>Invitaciones</td>
							<td>Invitaciones</td>
							<td>Invitaciones</td>
						</tr>
						<tr class="basket-icon">
							<td>Decoraci�n ayto / iglesia</td>
							<td>Decoraci�n ayto / iglesia</td>
							<td>Decoraci�n ayto / iglesia</td>
						</tr>
						<tr class="star-icon">
							<td>Servicio florister&iacute;a</td>
							<td>Servicio florister&iacute;a</td>
							<td>Servicio florister&iacute;a</td>
						</tr>
						<tr class="heart-icon">
							<td>Alquiler de local</td>
							<td>Alquiler de local</td>
							<td>Alquiler de local</td>
						</tr>
						
						<tr class="clock-icon">
							<td>Servicio de catering</td>
							<td>Servicio de catering</td>
							<td>Servicio de catering</td>
						</tr>
						<tr class="basket-icon">
							<td>Tarta nupcial</td>
							<td>Tarta nupcial</td>
							<td>Tarta nupcial</td>
						</tr>
						<tr class="star-icon">
							<td>Reportaje fotogr&aacute;fico</td>
							<td>Reportaje fotogr&aacute;fico / video</td>
							<td>Reportaje fotogr&aacute;fico / video</td>
						</tr>
						<tr class="heart-icon">
							<td> </td>
							<td>Alquiler trajes novio / a</td>
							<td>Confecci&oacute;n trajes novio / a</td>
						</tr>
						<tr class="heart-icon">
							<td> </td>
							<td>Alquiler veh&iacute;culo</td>
							<td>Alquiler veh&iacute;culo</td>
						</tr>
						<tr class="heart-icon">
							<td> </td>
							<td> </td>
							<td>Despedida soltero / a</td>
						</tr>
						<tr class="heart-icon">
							<td> </td>
							<td> </td>
							<td>Orquesta</td>
						</tr>
						<tr class="heart-icon">
							<td> </td>
							<td> </td>
							<td>Viaje de novios</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td><a onclick="abrirVentana('Contratacion Bodas Pack Ahorro')" class="verde" href="#">Contratar</a></td>
							<td><a onclick="abrirVentana('Contratacion Bodas Pack Basico')" class="verde" href="#">Contratar</a></td>
							<td><a onclick="abrirVentana('Contratacion Bodas Pack Lujo')" class="verde" href="#">Contratar</a></td>
						</tr>
					</tfoot>
				</table>
				<br/>
			</section>
			<footer>
				<?php
					include('./scripts/php/footer.php');
				?>
			</footer>
		</section>
		<?php
			include('./scripts/php/scripts.php');
		?>
	</body>
</html>