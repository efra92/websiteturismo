<!doctype html>
<html lang="es">
	<head>
		<title>&copy;Web oficial Edma Events</title>
		<link rel="shortcut icon" href="./favicon.ico">
	</head>
	<body>
		<section id="wrapper">
			<section id="sueno">
				<img src="./styles/img/realidad.png"/>
			</section>
			<header>
				<?php
					include('./scripts/php/header.php');
				?>
				<section id="menus">
				<?php
					include('./scripts/php/menu.php');
				?>
				</section>
			</header>
			<section id='slider'>
				<?php
					include('./scripts/php/slider.php');
				?>
			</section>
			<section id='content'>
				<?php
					include('./scripts/php/features.php');
				?>
			</section>
			<footer>
				<?php
					include('./scripts/php/footer.php');
				?>
			</footer>
		</section>
		<?php
			include('./scripts/php/scripts.php');
		?>
	</body>
</html>