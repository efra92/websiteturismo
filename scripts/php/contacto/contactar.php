<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
 
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<link rel="stylesheet" href="contacto.css" type="text/css" media="all" />  
	<script src="./../../../scripts/js/validacion/validacion.js" type = "text/javascript"></script>	
	
	<style type = "text/css">
			.etiqueta{
			
					color:#000;
					border-radius: 5px;				
					text-align:center;
					font-family:Arial;
					padding : 5px;
					margin-bottom: 10px;
					
			}
			
			.cierre{
			
				text-align : center;
				
			}
			
			fieldset{					
					border-radius: 5px;
			}
			
			legend{
					margin-left : 50px;
			}
			
		</style>
	
</head> 
<body>

		<div class = "caja">
						
			<form  id = "miformulario" name = "miformulario" action = "#" method = "post">
						
				<fieldset border = "1px solid orange">  
								
					<legend class = "etiqueta">Contacto con EDMA EVENTS</legend>

						<table>
						
							<tr>
							
								<td>
								
									<label>Destino: </label>
							
								</td>
								
								<td>
							
									<input type = "text" id = "txtDestin"  name = "txtDestin" size = "40px" value = "edmaeventsinfo@edmaevents.com" readonly = "readonly" disabled = "disabled"/>
									
								</td>
								
								<td>
								
									<div id = "infoDestin"></div>
							
								</td>
								
							</tr>	
						
							<tr>
							
								<td>
								
									<label>Remitente: </label>	
									
								</td>
								
								<td>
								
									<input type = "text" id = "txtRemitent" name = "txtRemitent" size = "40px" value="Introduce aqu&iacute tu email" onfocus="this.value='';" style="color:grey; font-style:oblique; font-size:11px;" />
									
								</td>
								
								<td>
								
									<div id = "infoRemitent" style="color:red;">*</div>
							
								</td>
								
							</tr>	
						
							<tr>
							
								<td>
								
									<label>Asunto: </label>	
									
								</td>
								
								<td>	
									
									<?php
									
										echo "<input type='text' id='txtTitle' name='txtTitle' size='40px' value='" .$_GET['evento']. "' readonly='readonly' disabled='disabled'/>";
									
									?>
								
								</td>
								
								<td>
								
									<div id = "infoTitle"></div>
							
								</td>
								
							</tr>	
						
							<tr>
							
								<td>
								
									<label>Mensaje: </label>	
									
								</td>
						
								<td>	
						
									<textarea id = "txtContent" cols = "32" style="color:grey; font-style:oblique; font-size:11px;"></textarea>
						
								</td>
								
								<td>
								
									<div id = "infoContent" style="color:red;">*</div>
							
								</td>
						
							</tr>							
							
						</table>						
						
						<div style="text-align:center;">
				
							</br>
												
							<input type = "submit" value ="Enviar" class="submit"/>
						
							&nbsp;
						
							<input type = "reset" id = "btnRestablecer" value = "Borrar" class="reset"/>
									
							</br>												
							
						</div>	
						
				</fieldset>				
				
				<div class = "cierre" style="text-align:center;">
		
					<a href="#" onclick="window.close()" style="color:black; font-size:10px;">Cerrar esta ventana</a>
			
				</div>
		
			</form>
						
		</div>
		
	</body>
	
</html>