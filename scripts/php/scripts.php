	<!-- Archivos CSS -->
	<link rel="stylesheet" type="text/css" href="./styles/css/slider/demo.css" />
	<link rel="stylesheet" type="text/css" href="./styles/css/slider/style.css" />
	<link href="./styles/css/main-stylesheet.css" type="text/css" rel="stylesheet" />
	<link rel="stylesheet" href="./styles/css/jqueryUi/jquery-ui.css" />
	<link rel="stylesheet" href="./styles/css/menu/style.css" />
	<link href="./styles/css/colorbox/colorbox.css" rel="stylesheet" />
	<link href="./styles/css/lightbox/lightbox.css" rel="stylesheet" />
	<link href="./styles/css/calendar/style.css" rel="stylesheet" />
	<!-- Scripts Javascript -->
	<script src="./scripts/js/jquery-1.7.2.min.js"></script>
	<script src="./scripts/js/colorbox/jquery.colorbox.js"></script>
	<script src="./scripts/js/lightbox/lightbox.js"></script>
	<!-- Scripts Jquery -->
	<script type="text/javascript" src="./scripts/js/slider/jquery.eislideshow.js"></script>
	<script type="text/javascript" src="./scripts/js/slider/jquery.easing.1.3.js"></script>
	<script src="./scripts/js/jqueryUi/jquery-ui.js"></script>
	<script>
		// En este apartado se encargará de cambiarle la hoja de estilos al navegador de Chrome porque le cambie algunas cosas.
		var chrome = (navigator.appVersion.toLowerCase().indexOf('chrome') > -1);
			if (chrome)
			document.write("<link href=\"./styles/css/estiloChrome.css\" rel=\"stylesheet\" type=\"text/css\" />");
	</script>
	<script>
		function mostrarSueno(){
			obj = document.getElementById('sueno');
			obj.style.display = 'block';
		}
		function ocultarSueno(){
			obj = document.getElementById('sueno');
			obj.style.display = 'none';
		}
	</script>
	<script>
			$(function() {
				$( "#tabs" ).tabs();
			});
		</script>
	<script>
		$(document).ready(function(){
			//Examples of how to assign the ColorBox event to elements
			$(".iframe").colorbox({iframe:true, width:"655px", height:"620px"});
		});
	</script>
	<script type="text/javascript">
		function initMenu() {
			var block = $(".day");
				block.addClass("clickable");
				block.hover(function(){window.status = $(this)}, function(){window.status = ""});
			
			$('.abierto').hide();
			block.click(
				function() {
					$(this).parents('section:eq(0)').find('.abierto').slideToggle('fast');	
				}
			);}
		$(document).ready(function() {initMenu();});
   	</script>
	<script type="text/javascript">
		$(function() {
			$('#ei-slider').eislideshow({
				animation			: 'center',
				autoplay			: true,
				slideshow_interval	: 3000,
				titlesFactor		: 0
			});
		});
		 $(function() {
			$( document ).tooltip();
		});
	</script>
	<script type="text/javascript">
		$(function() {
			// set opacity to nill on page load
			$("ul#menu span").css("opacity","0");
			// on mouse over
			$("ul#menu span").hover(function () {
				// animate opacity to full
				$(this).stop().animate({
					opacity: 1
				}, 'slow');
			},
			// on mouse out
			function () {
				// animate opacity to nill
				$(this).stop().animate({
					opacity: 0
				}, 'slow');
			});
		});
	</script>
	<script>
		$(document).ready(function() {           
			if (!Modernizr.input.placeholder)
			{		
				var placeholderText = $('#search').attr('placeholder');
				
				$('#search').attr('value',placeholderText);
				$('#search').addClass('placeholder');
				
				$('#search').focus(function() {				
					if( ($('#search').val() == placeholderText) )
					{
						$('#search').attr('value','');
						$('#search').removeClass('placeholder');
					}
				});
				
				$('#search').blur(function() {				
					if ( ($('#search').val() == placeholderText) || (($('#search').val() == '')) )                      
					{	
						$('#search').addClass('placeholder');					  
						$('#search').attr('value',placeholderText);
					}
				});
			}                
		});
	</script>
	<script type="text/javascript">
            function abrirVentana(cadena) {
				// declaramos las variables que recogeran las medidas de la posicion de la ventana
				var izq = ((screen.width / 2) - (500 / 2));
				var top = ((screen.height / 2) - (500 / 2));
				// abrimos la ventana secundaria centrada
				var secundaria = window.open("./scripts/php/contacto/contactar.php?evento=" + cadena, "", "width=460px, height=450px, top=" + top + ", left=" + izq + ", menubar=no, titlebar=no, menubar=no, status=no");
				// si no se abre la ventana mostramos mensaje de error
				if (secundaria == null)
					alert("No se ha podido cargar la pagina secundaria");		
				}
        </script>