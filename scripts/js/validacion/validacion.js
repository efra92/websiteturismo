
	// carga del evento load

	window.addEventListener("load", preparar, false);

	// FUNCION QUE PREPARA EL MANEJADOR DE EVENTOS

	function preparar(){

		var formulario = document.getElementById("miformulario");
		
		formulario.addEventListener("submit", validarDatos, false);
			
	}

	// FUNCION QUE VALIDA LA INTRODUCCION DE DATOS OBLIGATORIOS
		
	function validarDatos(evento){
	
		$correcto = true;
	
		// recogemos en variables los datos introducidos en el formulario
		
		var remitent = document.getElementById("txtRemitent").value;
		
		//var asunto = document.getElementById("txtTitle").value;
		
		var content = document.getElementById("txtContent").value;

		// declaramos las expresiones regulares para el campo email remitente
		
		var expEmail = /(^[0-9a-zA-Z]+(?:[._][0-9a-zA-Z]+)*)@([0-9a-zA-Z]+(?:[._-][0-9a-zA-Z]+)*\.[0-9a-zA-Z]{2,3})$/;
						
		// controlamos la direccion email del remitente	
			
		if (expEmail.test(remitent))
				
			document.getElementById("infoRemitent").innerHTML = "<img src='./images/correcto.jpg'/>";
							
		else{
						
			document.getElementById("infoRemitent").innerHTML = "<img src='./images/no.gif'/>";
			
			$correcto = false;
							
			evento.preventDefault();
												
		}	
		
		// controlamos el contenido del mensaje
				
		if (content != ""){
				
			document.getElementById("infoContent").innerHTML = "<img src='./images/correcto.jpg' />";
			
			evento.preventDefault();}
			
		else{
		
			document.getElementById("infoContent").innerHTML = "<img src='./images/no.gif' />";
			
			$correcto = false;
		}
			
		if ($correcto){
			alert("Los datos son correctos y procedemos a enviar el correo...");
			alert("Correo enviado.");
			document.getElementById("txtContent").value = "";
			document.getElementById("txtRemitent").value = "";
			document.getElementById("infoContent").innerHTML = "*";
			document.getElementById("infoRemitent").innerHTML = "*";
		}
		
		else
			alert("Revisa los campos a rellenar porque falla algo.");
		
	}
